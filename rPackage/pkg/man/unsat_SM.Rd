\name{unsat_SM}
\docType{methods}
\alias{unsat_SM}
\title{Soil water balance model (generalised for multibucket and unsaturated flow)}
\description{This is a soil water balance model that takes inputs such as rain and irrigation, coupled with outputs such as evapotranspiration and drainage to model the status of soil water at given time intervals. Function runs on a single time step. Function is agnostic to the length of the time step.}

\usage{unsat_SM(
  Rainfall = NULL, 
  ET = NULL,
  Irrigation = NULL, 
  MoistureLevs = NULL, 
  DrainageParam = NULL, 
  bucketSize = NULL, 
  Time = NULL)}

\arguments{
  \item{Rainfall}{Numeric; amount of rainfall in the given time step (mm).} 
  \item{ET}{Numeric; amount of evapotranspiration in the given time step (mm).}     
  \item{Irrigation}{Numeric; amount of irrigation in the given time step (mm).}
  \item{MoistureLevs}{List; Rolling soil moisture levels from 1 time step to the next. This list has a defined number of elements, depending on the number of buckets (soil depth layers at which soil moisture is modelled) are used. See the data \code{MoistureLevs} for an example of this object. Depending on how many buckets are used, the first \code{1:number of buckets} elements will be soil moisture levels for each bucket. Each bucket is expressed as a vector with three elements, where the first element is the current soil moisture level, the second element is the amount of water to travel out of the bucket in the next time step, and the third element is the amount of water to be added in the current time step. The last 3  elements within \code{MoistureLevs} are spaces for the total profile soil moisture, amount of runoff, and amount of deep drainage respectively that accrued in the present time step.}
  \item{DrainageParam}{List; Pertinent parameters of the soil moisture drainage model. This list has four elements. The length of each element will be equivalent to the number of buckets considered in the model. See the data \code{DrainageParam} for a look at how the object would be in the instance of a three bucket model. The first element corresponds to the a percentage of current water levels to remain in the bucket for the next time step. There will be a value for each bucket. The second element is essentially 1 - the values in the first element. This corresponds to the amount that moves out of the bucket for the next time step. The third element values correspond to the proportion of rainfall that enters each bucket for each time step. This sums to 1 for all buckets. The fourth element is the same as for rainfall except this is the proportion of evapo-transpiration taken out of each bucket.}
  \item{bucketSize}{Vector; each bucket will have an estimated bucket size that will have been measured or estimated. The \code{soilTheta} function provides a means to calculate bucket size using inputs of clay, sand and bulk density.}
  \item{Time}{Character; Some expression of time. Not really used in the function, but more for labelling time stamps. Becomes relevant when processing a time series of data.}
  \item{ResSM}{Numeric; A value representing residual soil moisture. This is expressed as a percentage of bucket size. On drying down, soil moisture can not get below this proportion of the bucket size.}
}


\value{Returns a list with three elements. Element 1: The time stamp. Element 2: A vector of recorded Rainfall, irrigation, ET, Rainfall-ET deficit, runoff, deep drainage, and soil moisture levels for each bucket. Element 3 is a list and is the updated form of \code{Moisturelevs}, the rolling soil moisture tallies for each bucket to be used in a subsequent time step advance of the model.}


\note{Typically the values of \code{DrainageParam} would be optimised rather than arbitrarily selected. Inverse modelling is generally recommended to achieve these optimised parameters. }

\author{Brendan Malone }

\references{}

\examples{
# Example using soil water balance model with three buckets

# library(aqua)
# data(DrainageParam) # soil drainage parameters
# data(Moisturelevs) # rolling soil moisture levels

# INPUTS
# Rainfall<- 2.296875 #rainfall (mm)
# Irrigation<- 0
# ET<- 2.741221 #ET
# bucketSize<- c(30.25777, 29.42490, 28.31781) # soil bucket sizes
# Time<- "2017-05-04T00:00:00"
# resSM<- 0.12 # residual soil moisture percentage of bucket size 

# Run Function
# TS1<- unsat_SM(Rainfall= Rainfall, 
#         ET = ET,
#         Irrigation= Irrigation,
#         MoistureLevs = MoistureLevs, 
#         DrainageParam = DrainageParam, 
#         bucketSize = bucketSize, 
#         Time = Time,
#         resSM = resSM)
# TS1}

\keyword{methods}
