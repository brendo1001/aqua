\name{ET0_dailyHarg}
\docType{methods}
\alias{ET0_dailyHarg}
\title{Reference Crop Evapotranspiration from Temperature (daily)}
\description{As an alternative when solar radiation data, relative humidity data and/or wind speed data are missing, reference evapotranspiration (ET0), can be estimated using the \href{http://elibrary.asabe.org/abstract.asp??JID=3&AID=26773&CID=aeaj1985&v=1&i=2&T=1}{Hargreaves equation}. This function estimates daily evaptranspiration.}

\usage{ET0_dailyHarg(Temp = NULL, 
    Loc = NULL, 
    Date = NULL}

\arguments{

  \item{Temp}{vector; Minimum air temperature (Tmin) and Maximum air temperature (Tmax). Expected syntax: \code{c(Tmin,Tmax)}.} 
  
  \item{Loc}{vector; Location in decimal degrees. Expected syntax: c(lat, long). Notes: Latitude (+ to N); Longitude (+ to E)}     
  
  \item{Date}{vector; Expressed as: \code{c(day, month, year)}.}}

\value{Returns a numeric estimate of the daily reference evapotranspiration (mm) using the Hargreaves equation.}


\note{The reason the function requires data about location and date is for the estimation of the extra-terrestrial radiation value that is used in the Hargreaves equation. The Hargreaves equation is for all intents and purposes the same when applied at different time steps such as hourly and sub-hourly steps. The only difference is in the estimation of the extra-terrestrial radiation.}

\author{Brendan Malone }

\references{
\itemize{
  \item Allen, R.C., Pereira, L.S., Raes, D., Smith, M. (1998) \href{http://www.fao.org/docrep/X0490E/x0490e00.htm#Contents}{Crop evapotranspiration - Guidelines for computing crop water requirements - FAO Irrigation and drainage paper 56}. FAO - Food and Agriculture Organization of the United Nations, Rome.
  \item Hargreaves, G.H., Samani, Z.A., (1985) \href{http://elibrary.asabe.org/abstract.asp??JID=3&AID=26773&CID=aeaj1985&v=1&i=2&T=1}{Reference crop evapotranspiration from temperature}. Applied Engineering in Agriculture. 1(2): 96-99.
  }
}

\examples{
## Not Run ##
# library(aqua)
#
# Location near Wagga Wagga, NSW 
# Loc<- c(-35.129379, 147.288546) 
#
# Date
# Date<- c(18,6, 2018) #day, month, year
#
# Temperature
# Temp<- c(2,15)
#
# Run function
# predETO<- ET0_dailyHarg(Date = Date,Loc = Loc,Temp = Temp)
}

\keyword{methods}
