\name{ET0_day}
\docType{methods}
\alias{ET0_day}
\title{Calculate reference evapotranspiration for hourly and sub-hourly time steps.}
\description{The \href{http://www.fao.org/docrep/X0490E/x0490e00.htm#Contents}{Penman-Monteith} equation is used for estimating referece crop evapotranspiration (ET0). This is calculated on hourly and sub-hourly time steps.}

\usage{ET0_shour(
  Tmean = NULL,
  rhMean = NULL,
  u2 = NULL,
  SolarRad = NULL,
  altitude = NULL,
  Loc = NULL,
  Date = NULL,
  Sun = NULL
)}

\arguments{

  \item{Tmean}{numeric; Mean temp over the measured time period.} 
  
  \item{rhMean}{numeric; Mean relative humidity over the measured time period..}     
  
  \item{SolarRad}{numeric; Measured solar radiation. Units of measurment are: MJ m2 per unit time.}     
  
  \item{u2}{numeric; Wind speed measured at 2m height (m/s).}     
  
  \item{altitude}{numeric; height above sea level (m)}     
  
  \item{Loc}{vector; Location in decimal degrees. Expected syntax: c(lat, long). Notes: Latitude (+ to N); Longitude (+ to E)}     
  
  \item{Date}{list; Information regarding time parameters. An output of the \code{timeStrings} function.}     
  
  \item{Sun}{vector; To the nearest hour, the hours of sunrise and sunset for the given day of interest. This could be estimated using the \code{sunTime} function.}}     



\value{Returns a numeric estimate of the reference evapotranspiration (mm) for the given time period.
}

\note{Sunrise and sunset hours are required for the estimation of the soil heat flux, which is used in the estimation of evapotranspiration. Expressed in 24-hour time}

\author{Brendan Malone }

\references{
\itemize{
  \item Allen, R.C., Pereira, L.S., Raes, D., Smith, M. (1998) \href{http://www.fao.org/docrep/X0490E/x0490e00.htm#Contents}{Crop evapotranspiration - Guidelines for computing crop water requirements - FAO Irrigation and drainage paper 56}. FAO - Food and Agriculture Organization of the United Nations, Rome.}
}

\examples{
## Not Run ##
# library(aqua)
# Location near Wagga Wagga, NSW 
# Loc<- c(-35.129379, 147.288546) 
#
# Date (need to use timeStuff function)
# Date<- timeStuff<- timeStrings(Date1= c(18,6, 2018),Date2=c(18,6, 2018), step1 = "9:00:00", step2 = "9:15:00")
#
# Windspeed
# u2<- 1.2 #(m/s)
#
# Temperature
# Tmean<- 5
#
# Humidity
# rhMean<- 94
#
# Solar radiation
# SolarRad <- 0.584009 # (MJ m2 unit time)
#
# Altitude
# altitude<- 147

# Sunshine hours
# Sun<- c(7,17)
#
# Run function
# et0_est<- ET0_shour(
#  Tmean =  Tmean,
#  rhMean =  rhMean,
#  u2 =  u2,
#  SolarRad = SolarRad,
#  altitude = altitude,
#  Loc = Loc,
#  Date = Date,
#  Sun<- Sun)
}

\keyword{methods}
