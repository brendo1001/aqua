\name{MoistureLevs}
\docType{data}
\alias{MoistureLevs}
\title{Rolling soil moisture levels example.}

\description{Example of soil moisture levels (\code{MoistureLevs}) input for the \code{unsat_SM} function. Just provides users of how the \code{MoistureLevs} input should look.}

\usage{data(MoistureLevs)}

\format{
\code{MoistureLevs} is a list whose number of elements is determined by the number of layers or buckets are considered in the soil water balance model. The given example shows what the object would look like with three buckets. Generally the first \code{1:number of buckets} elements will be soil moisture levels for each bucket. Each bucket is expressed as a vector with three elements, where the first element is the current soil moisture level, the second element is the amount of water to travel out of the bucket in the next time step, and the third element is the amount of water to be added in the current time step. The last 3  elements within \code{MoistureLevs} are spaces for the total profile soil moisture, amount of runoff, and amount of deep drainage respectively that accrued in the present time step.}

\details{}


\examples{
## Not Run ##
# library(aqua)
# data(MoistureLevels)
# MoistureLevs
}
\keyword{datasets}
