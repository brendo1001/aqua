###########################################################################################################################################
## FUNCTION
## Estimates ET0 with data on an hour or sub-hourly timescale
## Penman-Monteith Method

### Inputs
# Tmean: mean hourly temp
# rhMean: mean hourly relative humidity
# u2: wind speed (m/s) 2m above ground surface
# SolarRad: total solar radiation
# altitude: Altitude above sea level
# Loc: location in decimal degrees c(lat, long). Notes: Latitude (+ to N); Longitude (+ to E)
# Date: list output from the timeStrings function. Contains date and time information plus some other time derived parameters for ET0 calculation
# Sun: hours of sunrise and sunset on given date

#Returns
# An estimate ET0 value for the given time interval (UNITS of the Data: mm per unit time)



ET0_shour<- function(
  Tmean = NULL, #mean hourly temp
  rhMean = NULL, # % #mean hourly relative humidity
  u2 = NULL,  #wind speed
  SolarRad = NULL, #total solar radiation
  altitude = NULL, # Altitude
  Loc = NULL, # location in decimal degrees c(lat, long). Notes: Latitude (+ to N); Longitude (+ to E)
  Date = NULL, # list output from the timeStrings function
  Sun = NULL # hours of sunrise and sunset on given date
)
{
  
  lz.coeff<- seq(0,345,15) # degrees west of Greenwich
  
  ## Calculated parameters
  # Slope of saturation vapour pressure curve (delta)  
  delta<- (4098 * (0.6108*exp((17.27*Tmean)/(Tmean+237.3))))/((Tmean+237.3)^2)
  
  ## From Equation 7: Average value of atmospheric pressure (kPa)
  P<- 101.3 *((293 - (0.0065 * altitude ))/293)^5.26
  
  ## From Equation 8: The psychrometric constant (kPa C-1)
  phc<- 0.655 * 10^-3 * P
  
  # Vapour pressure deficit
  #mean saturation vapour pressure (es) (equation)
  es<- 0.6108*exp((17.27*Tmean)/(Tmean+237.3))  #equation 11
  ea<- es*(rhMean/100)
  vpd<- es-ea
  
  #  extraterrestrial radiation (Ra ) for hourly or shorter periods
  #RADIATION 
  # day of the year 
  J<- yday(timeStuff[[1]][1]) # Using function from Lubridate
  rad<- pi/180 * Loc[1] # location radians latitude
  dr<- 1 + 0.033 * cos(((2*pi)/365)*J) # inverse relative distance Earth-Sun
  sd<- 0.409 * sin(((2*pi)/365)*J-1.39)#  solar declination
  
  #seasonal correction for solar time
  b<- (2*pi * (J-81))/ 364
  sc<- 0.1645*sin(2*b) - 0.1255*cos(b)-0.025*sin(b)
  
  #estimation of longitude time zone
  L1<- abs(lz.coeff - abs(Loc[2]))
  L2<- min(L1)
  lz<- lz.coeff[which(L1==L2)]
  Lm<- abs(Loc[2])
  
  #standard clock time (midpoint time)
  t<- Date[[1]][1] + ((Date[[2]][1]/2)*60)
  t<- strptime(t, format="%Y-%m-%d %H:%M")
  t<- t$hour+ t$min/60
  
  w<- (pi/12)*((t+0.0667*(lz-Lm)+sc)-12) #solar time angle at midpoint
  w1<- w-((pi*Date[[2]][2])/24)
  w2<- w+((pi*Date[[2]][2])/24)
  
  Ra<- ((12*60)/pi)* (0.0820*dr) * ((w2 - w1) * sin(rad) * sin(sd)+cos(rad)*cos(sd)*(sin(w2)-sin(w1)))
  if (Ra < 0){Ra<-0}
  
  #Radiation
  Rs0<- (0.75 + 2* 10^-5) * Ra #clear sky radiation
  Rns<- 0.77 * SolarRad # net solar or shortwave radiation 
  Rsr<-SolarRad/Rs0
  if(is.finite(Rsr) == FALSE) {Rsr<- 0.8}
  
  TmeanK<- ((4.903 * 10^-9) * (Tmean + 273.16)^4)/Date[[2]][4]
  rnl2<- 0.34 - 0.14 * sqrt(ea)
  rnl3<- 1.35 * Rsr - 0.35
  Rnl<- TmeanK * rnl2* rnl3
  Rn<- Rns - Rnl
  
  # Soil heat flux
  if (timeStuff[[1]][1]$hour < Sun[2] & timeStuff[[1]][1]$hour > Sun[1]  ) # daylight
  {Gp<- 0.1 * Rn} else {Gp<- 0.5 * Rn}
  Gdiff<- Rn - Gp
  Gequiv<- 0.408 * Gdiff
  
  # Grass reference evapotranspiration 
  gre1<- Gequiv * delta / (delta + phc * (1 + 0.34 *u2))
  gre2<-  37/ (Tmean+273) * u2 * vpd * (phc / (delta + phc * (1 + 0.34 *u2)))
  ET0<- gre1 + gre2 # mm/day
  if(ET0 < 0){ET0<- 0}
  return(ET0)
}