
# get historical weather information for a site
# Use the bomrang R package and does a bit of extra curation.
# Curation included combining min temp, max temp, solar and rainfall data rather than having 4 seperate data frames
# Retruns combine weather data that ensures all 4 variables are observed
# Instances of NA values are handled as such:
  # min and temp and solar: the long term average is applied
  # rainfall: treated as zero
## All the other features of the bomrang package are used. User simply puts in the coordinates where they wish to get data from. 
## This function does the rest ie extracts the historical data from the nearest bom station 

## Use function
#test1<- BOM_dat_historical(latlon= c(-35.1583, 147.4575))


# Begin function
BOM_dat_historical<- function(latlon=NULL){
  d1_min<- get_historical(latlon = latlon,type = "min") 
  d1_max<- get_historical(latlon = latlon,type = "max")
  d1_rain<- get_historical(latlon = latlon,type = "rain")
  d1_solar<- get_historical(latlon = latlon,type = "solar")


julianDay<- function(dat){
# c(Year, Month, Day, hour)
dat$a<- floor((14- dat$Month)/12)
dat$y<- dat$Year + 4800 - dat$a  #years since March 1 4801 BC
dat$m<- dat$Month + (12*dat$a) - 3 #months since March 1 4801 BC
dat$JDN<- dat$Day + floor(((153*dat$m) + 2)/5) + 365*dat$y + floor(dat$y/4) - floor(dat$y/100) + floor(dat$y/400) -  32045 # Julian Day number
return(dat)}

# julian day
d1_min<- julianDay(dat=d1_min)
d1_max<- julianDay(dat=d1_max)
d1_rain<- julianDay(dat=d1_rain)
d1_solar<- julianDay(dat=d1_solar)

# subset matching data
#min
d1_min$s1<- as.numeric(d1_min$JDN %in% d1_max$JDN)
d1_min$s2<- as.numeric(d1_min$JDN %in% d1_rain$JDN)
d1_min$s3<- as.numeric(d1_min$JDN %in% d1_solar$JDN)
d1_min$s4<- d1_min$s1 +  d1_min$s2 + d1_min$s3
d1_min<- d1_min[d1_min$s4 == 3,]

#max
d1_max$s1<- as.numeric(d1_max$JDN %in% d1_min$JDN)
d1_max$s2<- as.numeric(d1_max$JDN %in% d1_rain$JDN)
d1_max$s3<- as.numeric(d1_max$JDN %in% d1_solar$JDN)
d1_max$s4<- d1_max$s1 +  d1_max$s2 + d1_max$s3
d1_max<- d1_max[d1_max$s4 == 3,]

#rain
d1_rain$s1<- as.numeric(d1_rain$JDN %in% d1_min$JDN)
d1_rain$s2<- as.numeric(d1_rain$JDN %in% d1_max$JDN)
d1_rain$s3<- as.numeric(d1_rain$JDN %in% d1_solar$JDN)
d1_rain$s4<- d1_rain$s1 +  d1_rain$s2 + d1_rain$s3
d1_rain<- d1_rain[d1_rain$s4 == 3,]

#solar
d1_solar$s1<- as.numeric(d1_solar$JDN %in% d1_min$JDN)
d1_solar$s2<- as.numeric(d1_solar$JDN %in% d1_max$JDN)
d1_solar$s3<- as.numeric(d1_solar$JDN %in% d1_rain$JDN)
d1_solar$s4<- d1_solar$s1 +  d1_solar$s2 + d1_solar$s3
d1_solar<- d1_solar[d1_solar$s4 == 3,]

# combine data sets
datF<- cbind(d1_min[,c(1:5)], d1_min[,c("JDN")], d1_min[,c("Min_temperature")],
             d1_max[,c("Max_temperature")], d1_solar[,c("Solar_exposure")],
             d1_rain[,c("Rainfall")]
             )
names(datF)[6:10]<- c("JulianDay", "minTemp", "maxTemp", "solarExposure", "rainfall") 

# curation
datF$rainfall[which(is.na(datF$rainfall))]<- 0 # change rainfall NAs to O
datF$solarExposure[which(is.na(datF$solarExposure))]<- mean(datF$solarExposure, na.rm=T) # change solar NAs to mean of solar
return(datF)}
