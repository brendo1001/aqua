# aqua
An R package for Soil moisture modelling and forecasting

#### *Actively under development*

# Contact
Brendan Malone

Email: <brendan.malone@sydney.edu.au>

# R package install
install.packages("devtools")

library(devtools)

install_bitbucket("brendo1001/aqua/pkg")

